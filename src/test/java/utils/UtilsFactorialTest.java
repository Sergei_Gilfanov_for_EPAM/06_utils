package utils;

import static org.junit.Assert.*;

import java.math.BigInteger;
import java.util.Random;

import org.junit.Ignore;
import org.junit.Test;

public class UtilsFactorialTest {
  
  @Test(expected=IllegalArgumentException.class)
  public void shouldDieForNegative() {
    Utils.fact(-1);
    fail("Negative argument: error exception was not thrown");
  }
  
  @Test
  public void shouldWorkWithLongRange() {
    BigInteger res = Utils.fact(20);
    // 20! = 2432902008176640000 еще меньше Long.MIN_VALUE == 2^63 - 1
    //                           == 9223372036854775807
    assertEquals(BigInteger.valueOf(2432902008176640000L), res);
  }
  
  @Test
  public void shouldNotOverflow() {
    BigInteger res = Utils.fact(21);
    // 21! = 51090942171709440000 уже больше Long.MIN_VALUE == 2^63 - 1
    //                         == 9223372036854775807
    assertEquals(new BigInteger("51090942171709440000"), res);
  }

  @Ignore("Disabled by request") @Test(timeout=10000)
  public void testFactorialWithTimeout() {
    int random = (new Random()).nextInt();
    BigInteger a = Utils.fact(random);
    BigInteger b = Utils.fact(random);
    assertEquals(a, b);
  }
}
