package utils;

import static org.junit.Assert.*;

import org.junit.Test;

public class UtilsConcatinateTest {
   
  @Test
  public void shouldWorkInBaseCase() {
    String res = Utils.concatenateWords("first", "second");
    assertEquals("firstsecond", res);
  }
  
  @Test(expected=IllegalArgumentException.class)
  public void shouldDieWhenFirstIsNull() {
    Utils.concatenateWords(null, "second");
    fail("(null).concat(String): error exception was not thrown");
  }

  @Test(expected=IllegalArgumentException.class)
  public void shouldDieWhenSecondISNull() {
    Utils.concatenateWords("first", null);
    fail("(null).concat(String): error exception was not thrown");
  }  
  
  @Test
  public void shouldCOncatenateEmptyFirst() {
    String res = Utils.concatenateWords("", "second");
    assertEquals("second", res);
  }

  @Test
  public void shouldCOncatenateEmptySecond() {
    String res = Utils.concatenateWords("first", "");
    assertEquals("first", res);
  }

  @Test
  public void shouldNotNormalizeComposites() {
    // композитная Ё 
    String res = Utils.concatenateWords("\u0415", "\u0308");
    // Простая Ё
    assertNotEquals("\u0401", res);
  }
  
  @Test
  public void shouldNotBreakSupplementary() {
    String res = Utils.concatenateWords("\uD801", "\uDC00");
    int bigCharInRes = res.codePointAt(0);
    assertEquals(0x10400, bigCharInRes);
  }
}
