package utils;

import java.math.BigInteger;

public class Utils {
  public static String concatenateWords(String a, String b) {
    // Неэффективная ручная реализация в учебных целях
    if (a == null || b == null) {
      throw new IllegalArgumentException();
    }
    int lenA = a.length();
    int lenB = b.length();
    char[] res= new char[lenA + lenB];
    int currentPos = 0;
    for ( int i = 0; i < lenA; ++i) {
      res[currentPos++] = a.charAt(i);
    }
    for ( int i = 0; i < lenB; ++i) {
      res[currentPos++] = b.charAt(i);
    }
    // (!) при конструировании создается копия массива.
    return new String(res);
  }
  
  public static BigInteger fact(int i) {
    if ( i < 0 ) {
      throw new IllegalArgumentException();
    }
    BigInteger res = BigInteger.valueOf(1);
    if ( i == 0 || i == 1) {
      return res;
    }
    while (i > 1) {
      res = res.multiply(BigInteger.valueOf(i--));
    }
    return res;
  }
  
  public static void main(String arg[]) {
    System.out.println(fact(Integer.MAX_VALUE));
  }
}

  